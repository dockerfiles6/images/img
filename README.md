# Premier build

```bash
export REGISTRY=<votre registry docker>
build.sh
```

En sortie vous aurez le nom de l'image qu'il vous suffira de pousser sur votre
registry docker.

```bash
docker login -u <user> -p <mdp> <adresse de votre registry>
docker push <image>
```