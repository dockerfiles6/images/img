#!/usr/bin/env sh
export PRODUCT=img
export SOURCE=genuinetools/img

pip install lastversion --user

export VERSION=$(lastversion ${SOURCE})

docker build --build-arg VERSION=${VERSION} -t ${REGISTRY}/${PRODUCT}:${VERSION} .
